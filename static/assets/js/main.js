/* Make sidebar sticky when scrolling */
$(function() {
    var $window = $(window);
    var $sideContent = $('aside');
    var $sidebar = $sideContent.parent();
    $window.scroll(function() {
			if ($(window).scrollTop() >= $('header').outerHeight()) {
                $sideContent.addClass('scrolling');
                $sidebar.addClass('scrolling');
      } else {
        $sideContent.removeClass('scrolling');
        $sidebar.removeClass('scrolling');
      }
    })
});