from flask import Flask, render_template
from http import HTTPStatus

app = Flask(__name__)

from utils.bundle import assets
from blueprints.back import back_app
from blueprints.front import front_app

app.register_blueprint(back_app)
app.register_blueprint(front_app)

@app.route('/tutu')
def tutu():
	return render_template('tutu.jinja')

@app.route('/add/<int:left>/<int:right>')
def add(left, right):
	return str(left + right)

@app.errorhandler(404)
def error404(e):
	return '404 not found'
