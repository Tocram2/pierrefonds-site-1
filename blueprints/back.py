from flask import Blueprint, request
from http import HTTPStatus

back_app = Blueprint('back_app', __name__)

from schemas.input.restaurant import restaurantAdd_schema
from marshmallow.exceptions import ValidationError

@back_app.route('/hello')
def hello():
	return 'Hello'

@back_app.route('/hello', methods=['POST'])
def helloPost():
    try:
        data = restaurantAdd_schema.load(request.get_json())
        return data['name']
    except ValidationError:
        return 'Bad data', HTTPStatus.BAD_REQUEST
	#return request.get_json()

@back_app.route('/hello/<name>')
def helloUser(name):
	return 'Hello {} !'.format(name), HTTPStatus.CREATED

# restaurants

@back_app.route('/admin/restaurants')
def restosGet():
    return "restosGet"

@back_app.route('/admin/restaurant/<int:resto_id>', methods=['PUT'])
def restoPut():
    try:
        data = restaurantAdd_schema.load(request.get_json())
        return data['name']
    except ValidationError:
        return 'Bad data', HTTPStatus.BAD_REQUEST
	
@back_app.route('/admin/restaurant/<int:resto_id>', methods=['DELETE'])
def restoDelete():
    return "restoDelete"

@back_app.route('/admin/restaurant/<int:resto_id>', methods=['UPDATE'])
def restoUpdate():
    return "restoUpdate"

@back_app.route('/admin/restaurant/<int:resto_id>')
def restoGet():
    return "restoGet"

# commerces

@back_app.route('/admin/commerces')
def commercesGet():
    return "commercesGet"

@back_app.route('/admin/commerce/<int:commerce_id>', methods=['PUT'])
def commercePut():
    try:
        data = commerceAdd_schema.load(request.get_json())
        return data['name']
    except ValidationError:
        return 'Bad data', HTTPStatus.BAD_REQUEST
	
@back_app.route('/admin/commerce/<int:commerce_id>', methods=['DELETE'])
def commerceDelete():
    return "commerceDelete"

@back_app.route('/admin/commerce/<int:commerce_id>', methods=['UPDATE'])
def commerceUpdate():
    return "commerceUpdate"

@back_app.route('/admin/commerce/<int:commerce_id>')
def commerceGet():
    return "commerceGet"

# monuments

@back_app.route('/admin/monuments')
def monumentsGet():
    return "monumentsGet"

@back_app.route('/admin/monument/<int:monument_id>', methods=['PUT'])
def monumentPut():
    try:
        data = monumentAdd_schema.load(request.get_json())
        return data['name']
    except ValidationError:
        return 'Bad data', HTTPStatus.BAD_REQUEST
	
@back_app.route('/admin/monument/<int:monument_id>', methods=['DELETE'])
def monumentDelete():
    return "monumentDelete"

@back_app.route('/admin/monument/<int:monument_id>', methods=['UPDATE'])
def monumentUpdate():
    return "monumentUpdate"

@back_app.route('/admin/monument/<int:monument_id>')
def monumentGet():
    return "monumentGet"

# hebergements

@back_app.route('/admin/hebergements')
def hebergementsGet():
    return "hebergementsGet"

@back_app.route('/admin/hebergement/<int:hebergement_id>', methods=['PUT'])
def hebergementPut():
    try:
        data = hebergementAdd_schema.load(request.get_json())
        return data['name']
    except ValidationError:
        return 'Bad data', HTTPStatus.BAD_REQUEST
	
@back_app.route('/admin/hebergement/<int:hebergement_id>', methods=['DELETE'])
def hebergementDelete():
    return "hebergementDelete"

@back_app.route('/admin/hebergement/<int:hebergement_id>', methods=['UPDATE'])
def hebergementUpdate():
    return "hebergementUpdate"

@back_app.route('/admin/hebergement/<int:hebergement_id>')
def hebergementGet():
    return "hebergementGet"

# loisirs

@back_app.route('/admin/loisirs')
def loisirsGet():
    return "loisirsGet"

@back_app.route('/admin/loisir/<int:loisir_id>', methods=['PUT'])
def loisirPut():
    try:
        data = loisirAdd_schema.load(request.get_json())
        return data['name']
    except ValidationError:
        return 'Bad data', HTTPStatus.BAD_REQUEST
	
@back_app.route('/admin/loisir/<int:loisir_id>', methods=['DELETE'])
def loisirDelete():
    return "loisirDelete"

@back_app.route('/admin/loisir/<int:loisir_id>', methods=['UPDATE'])
def loisirUpdate():
    return "loisirUpdate"

@back_app.route('/admin/loisir/<int:loisir_id>')
def loisirGet():
    return "loisirGet"

# medias

@back_app.route('/admin/medias')
def mediasGet():
    return "mediasGet"

@back_app.route('/admin/media/<int:media_id>', methods=['PUT'])
def mediaPut():
    try:
        data = mediaAdd_schema.load(request.get_json())
        return data['name']
    except ValidationError:
        return 'Bad data', HTTPStatus.BAD_REQUEST
	
@back_app.route('/admin/media/<int:media_id>', methods=['DELETE'])
def mediaDelete():
    return "mediaDelete"

@back_app.route('/admin/media/<int:media_id>', methods=['UPDATE'])
def mediaUpdate():
    return "mediaUpdate"

@back_app.route('/admin/media/<int:media_id>')
def mediaGet():
    return "mediaGet"

# autres

@back_app.route('/admin/login')
def adminLogin():
    return "adminLogin"

@back_app.route('/admin')
def adminAccueil():
    return "adminAccueil"
	
@back_app.route('/admin/mentions-legales')
def adminMentionsLegales():
    return "adminMentionsLegales"

@back_app.route('/admin/contact')
def adminContact():
    return "adminContact"

@back_app.route('/admin/avis')
def adminAvis():
    return "adminAvis"