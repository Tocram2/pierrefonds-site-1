from flask import Flask, Blueprint, render_template, redirect, url_for
from http import HTTPStatus
from utils.bundle import assets

front_app = Blueprint('front_app', __name__)

@front_app.route('/auth')
def login():
    return render_template('login.jinja')

@front_app.route('/register')
def register():
    return render_template('register.jinja')
    
@front_app.route('/home')
def home():
    return render_template('index.jinja')
    
@front_app.route('/news')
def news():
    return render_template('news.jinja')

@front_app.route('/restaurants')
def restaurants():
    return render_template('retaurants.jinja')

@front_app.route('/restaurant/<int:resto_id>')
def restaurant(resto_id):
    return render_template('retaurant.jinja', resto_id)

@front_app.route('/commerces')
def commerces():
    return render_template('commerces.jinja')

@front_app.route('/commerce/<int:commerce_id>')
def commerce(commerce_id):
    return render_template('commerce.jinja', commerce_id)

@front_app.route('/monuments')
def monuments():
    return render_template('monuments.jinja')

@front_app.route('/monument/<int:monument_id>')
def monument(monument_id):
    return render_template('monument.jinja', monument_id)

@front_app.route('/hebergements')
def hebergements():
    return render_template('hebergements.jinja')

@front_app.route('/hebergement/<int:hebergement_id>')
def hebergement(hebergement_id):
    return render_template('hebergement.jinja', hebergement_id)

@front_app.route('/loisirs')
def loisirs():
    return render_template('loisirs.jinja')

@front_app.route('/loisir/<int:loisir_id>')
def loisir(loisir_id):
    return render_template('loisir.jinja', loisir_id)

@front_app.route('/medias')
def medias():
    return render_template('medias.jinja')

@front_app.route('/media/<int:media_id>')
def media(media_id):
    return render_template('media.jinja', media_id)

@front_app.route('/contact')
def contact():
    return render_template('contact.jinja')

@front_app.route('/agenda')
def agenda():
    return render_template('agenda.jinja')

@front_app.route('/mentions-legales')
def mentionslegales():
    return render_template('mentions-legales.jinja')

@front_app.route('/')
def index():
    return redirect(url_for('front_app.home'), code=302)
