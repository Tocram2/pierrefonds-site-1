-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema pierrefonds
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema pierrefonds
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pierrefonds` DEFAULT CHARACTER SET utf32 ;
USE `pierrefonds` ;

-- -----------------------------------------------------
-- Table `pierrefonds`.`lieu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pierrefonds`.`lieu` (
  `IdLieu` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID du lieu.',
  `NomLieu` VARCHAR(100) NOT NULL COMMENT 'Nom du lieu.\nMaximum de 100 caractères.',
  `DescLieu` MEDIUMTEXT NULL DEFAULT NULL COMMENT 'Description texte du lieu.\nMaximum de 16.777.215 caractères.\n',
  `AdrLieu` VARCHAR(1000) NULL DEFAULT NULL COMMENT 'Adresse postale du lieu.\nMaximum de 1000 caractères.',
  `HorairesLieu` VARCHAR(500) NULL DEFAULT NULL COMMENT 'Horaires d\'ouverture du lieu.',
  `TelLieu` VARCHAR(16) NULL COMMENT 'Numéro de tléphone du lieu.\nMaximum de 16 caractères (pour numéros en \"+33# ## ## ## ##\" le cas le plus extrême en longueur).',
  `MailLieu` VARCHAR(320) NULL COMMENT 'Adresse e-mail de contact du lieu.\nMaximum de 320 caractères selon la RFC 3696.',
  PRIMARY KEY (`IdLieu`),
  UNIQUE INDEX `idrestaurant_UNIQUE` (`IdLieu` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pierrefonds`.`media`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pierrefonds`.`media` (
  `IdMedia` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID du média.',
  `CredMedia` VARCHAR(200) NULL COMMENT 'Crédits et droits d\'auteurs du média.\nMaximum de 200 caractères.',
  `IdLieu` INT UNSIGNED NULL COMMENT 'ID du Lieu en rapport avec ce média.',
  `DescMedia` VARCHAR(500) NULL COMMENT 'Description texte du média.\nMaximum de 500 caractères.',
  `Video` BIT(1) NOT NULL DEFAULT 0 COMMENT 'Indique si le média est une vidéo (=1) ou une photo (=0).',
  PRIMARY KEY (`IdMedia`),
  UNIQUE INDEX `IdMedia_UNIQUE` (`IdMedia` ASC) VISIBLE,
  INDEX `IdLieu_idx` (`IdLieu` ASC) VISIBLE,
  CONSTRAINT `cleLieuMedia`
    FOREIGN KEY (`IdLieu`)
    REFERENCES `pierrefonds`.`lieu` (`IdLieu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pierrefonds`.`utilisateur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pierrefonds`.`utilisateur` (
  `IdUtilisateur` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID de l\'utilisateur.',
  `MailUtilisateur` VARCHAR(320) NOT NULL COMMENT 'Adresse e-mail de l\'utilisateur.\nMaximum de 320 caractères selon la RFC 3696.',
  `MdpUtilisateur` MEDIUMBLOB NOT NULL COMMENT 'Mot de passe encrypté de l\'utlisateur.',
  `NomUtilisateur` VARCHAR(30) NOT NULL COMMENT 'Nom / pseudonyme de l\'utilisateur.\nMaximum de 30 caractères.',
  PRIMARY KEY (`IdUtilisateur`),
  UNIQUE INDEX `IdUtilisateur_UNIQUE` (`IdUtilisateur` ASC) VISIBLE,
  UNIQUE INDEX `MailUtilisateur_UNIQUE` (`MailUtilisateur` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pierrefonds`.`avis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pierrefonds`.`avis` (
  `IdAvis` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID de l\'avis.',
  `IdUtilisateur` INT UNSIGNED NOT NULL COMMENT 'Clé étrangère de l\'utilisateur qui a posté l\'avis.',
  `IdLieu` INT UNSIGNED NOT NULL COMMENT 'Clé étrangère du lieu sous lequel est l\'avis.',
  `DatePubliAvis` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date de publication de l\'avis.',
  `Visible` BIT(1) NOT NULL DEFAULT 1 COMMENT 'Indique si le commentaire est visible ou non.\nVisible (=1) par défaut, invisible (=0) si supprimé par la modération.',
  `TexteAvis` VARCHAR(1000) NOT NULL COMMENT 'Texte de l\'avis.\nMaximum de 1000 caractères.',
  PRIMARY KEY (`IdAvis`),
  UNIQUE INDEX `IdAvis_UNIQUE` (`IdAvis` ASC) VISIBLE,
  INDEX `cleUtilisateur_idx` (`IdUtilisateur` ASC) VISIBLE,
  INDEX `cleLieu_idx` (`IdLieu` ASC) VISIBLE,
  CONSTRAINT `cleUtilisateur`
    FOREIGN KEY (`IdUtilisateur`)
    REFERENCES `pierrefonds`.`utilisateur` (`IdUtilisateur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `cleLieuAvis`
    FOREIGN KEY (`IdLieu`)
    REFERENCES `pierrefonds`.`lieu` (`IdLieu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
