from marshmallow import Schema, fields, validate

class HebergementAddSchema(Schema):
    name = fields.String(required=True, validate=validate.Length(min=4))
    address = fields.String(required=False)

hebergementAdd_schema = HebergementAddSchema(many=False)