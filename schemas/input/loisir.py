from marshmallow import Schema, fields, validate

class LoisirAddSchema(Schema):
    name = fields.String(required=True, validate=validate.Length(min=4))
    address = fields.String(required=False)

loisirAdd_schema = LoisirAddSchema(many=False)