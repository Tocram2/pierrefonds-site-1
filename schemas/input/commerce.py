from marshmallow import Schema, fields, validate

class CommerceAddSchema(Schema):
    name = fields.String(required=True, validate=validate.Length(min=4))
    address = fields.String(required=False)

commerceAdd_schema = CommerceAddSchema(many=False)