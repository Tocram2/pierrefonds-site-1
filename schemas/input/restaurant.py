from marshmallow import Schema, fields, validate

class RestaurantAddSchema(Schema):
    name = fields.String(required=True, validate=validate.Length(min=4))
    address = fields.String(required=False)

restaurantAdd_schema = RestaurantAddSchema(many=False)