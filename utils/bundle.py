from flask_assets import Environment, Bundle
from app import app

# constant lists
BASICS_CSS = ('vendor/bootstrap/bootstrap.min.css',
            'assets/css/core.css')
BASICS_JS = ('assets/js/jquery-3.6.0.min.js',
            'vendor/popper.js/popper.min.js',
            'vendor/bootstrap/bootstrap.min.js',
            'assets/js/main.js')

bundles = {
    'base_css': Bundle (
        BASICS_CSS[0],
        BASICS_CSS[1],
        output='gen/base.css'), #,filters='cssmin'

    'home_css': Bundle (
        BASICS_CSS[0],
        BASICS_CSS[1],
        'assets/css/home.css',
        output='gen/home.css'), #,filters='cssmin'

    'login_css': Bundle (
        BASICS_CSS[0],
        BASICS_CSS[1],
        'assets/css/login.css',
        output='gen/login.css'), #,filters='cssmin'

    'base_js': Bundle (
        BASICS_JS[0],
        BASICS_JS[1],
        BASICS_JS[2],
        BASICS_JS[3],
        output='gen/base.js'), #,filters='jsmin'

    'login_js': Bundle (
        BASICS_JS[0],
        BASICS_JS[1],
        BASICS_JS[2],
        BASICS_JS[3],
        'assets/js/form-checking.js',
        output='gen/login.js') #,filters='jsmin'
}

assets = Environment(app)
assets.register(bundles)
